&emsp;&emsp;因为确实没有必要造轮子了（再加上也没时间研究），所以，这次的设计直接模仿自立创做的调试器，原链接在[**这里**](https://lceda.cn/jixin002/stm32f103c8t6_cmsis-dap)，做了一点微小的改动，应该会更好用\_(:3J∠)\_

&emsp;&emsp;实物这个样：

![](./Picture/实物图.jpg)

&emsp;&emsp;制作教程在[**这里**](https://segmentfault.com/a/1190000022677785)

&emsp;&emsp;PDF格式的原理图在[**这里**](https://gitee.com/stm32proj/CMSIS-DAP-STM32/blob/master/PCB/V0.2/SAST-DAP-SCH.pdf)

&emsp;&emsp;固件在[**这里**](https://gitee.com/SAST-E/CMSIS-DAP-STM32/tree/master/firmware)

&emsp;&emsp;下载固件可以用[**这个**](https://gitee.com/SAST-E/SAST-E-Tools/blob/master/MCU/ISP_Tools/FlyMcu.exe)

&emsp;&emsp;克隆本仓库到本地请先复制以下语句：

```
git clone https://gitee.com/SAST-E/CMSIS-DAP-STM32.git
```

&emsp;&emsp;然后如果没有git init过的话，在指定的存放位置右键打开git bash以后请先执行这个：

```
git init 
```

&emsp;&emsp;然后执行这个：

```
git clone https://gitee.com/SAST-E/CMSIS-DAP-STM32.git
```





&emsp;&emsp;git就是这么枯燥(´～`)